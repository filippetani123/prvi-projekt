import PG from 'pg'
import dotenv from 'dotenv'
dotenv.config()

//Kreiran bazen za uspostavljanje veze sa bazom podataka
const pool = new PG.Pool({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database:process.env.DB,
    password: process.env.DB_PASSWORD,
    port: parseInt(process.env.DB_PORT || '5432'),
    ssl: true
})

export const deleteKomentar = async (req,res) => {
  try {
    const {id_komentara} = req.params
    console.log(id_komentara)
    const results = await pool.query(`delete from komentari where id_komentara = ${id_komentara} returning *`)
    res.status(200).json(results.rows)
  } catch (error) {
    res.status(404).json({message: error.message});
  }
}

export const getEkipe = async (req, res) => {
  try {
    const results = await pool.query('SELECT * from ekipa');
    res.status(200).json(results.rows)
  } catch (error) {
    res.status(404).json({message: error.message});
  }
}

export const getTablica = async (req, res) => {
  try {
    const results = await pool.query('select * from ekipa order by bodovi desc, kos_razlika desc');
    res.status(200).json(results.rows)
  } catch (error) {
    res.status(404).json({message: error.message});
  }
}


export const getUtakmice = async (req, res) => {
  try {
    const results = await pool.query(`select id_utakmice, domacin.ime_ekipe as ime_domacina, gost.ime_ekipe as ime_gosta
      , broj_poena_domacina, broj_poena_gosta
      , zastavica_odigrano, broj_kola
      from utakmica join ekipa as domacin on utakmica.id_domacina = domacin.id_ekipe
       join ekipa as gost on gost.id_ekipe = utakmica.id_gosta`);
    res.status(200).json(results.rows)
  } catch (error) {
    res.status(404).json({message: error.message});
  }
}

export const getKomentari = async (req, res) => {
  try {
    const {id_utakmice} = req.params
    const results = await pool.query(`select domacin.ime_ekipe as domacin, gost.ime_ekipe as gost, broj_poena_domacina, broj_poena_gosta,
    korisnicko_ime, komentar, vrijeme, id_komentara
    from komentari join korisnik on komentari.id_korisnika = korisnik.id_korisnika 
      join utakmica on utakmica.id_utakmice = komentari.id_utakmice
      join ekipa as domacin on utakmica.id_domacina = domacin.id_ekipe
      join ekipa as gost on utakmica.id_gosta = gost.id_ekipe
      where utakmica.id_utakmice = ${id_utakmice}`);
    res.status(200).json(results.rows)
  } catch (error) {
    res.status(404).json({message: error.message});
  }
}

export const createKomentar = async (req, res) => {
  try {
    const {id_korisnika, id_utakmice, komentar} = req.body
    const results = await pool.query(`INSERT INTO public.komentari(
      id_korisnika, id_utakmice, komentar)
      VALUES (${id_korisnika}, ${id_utakmice}, '${komentar}');`);
    res.status(200).json(results.rows)
  } catch (error) {
    res.status(404).json({message: error.message});
    console.log(error.message)
  }
}

export const insertUtakmica = async (req, res) => {
  try {
    const {id_domacina, id_gosta, broj_poena_domacina, broj_poena_gosta, broj_kola } = req.body
    console.log(req.body)
    const results = await pool.query(`DO $$ DECLARE
    broj_poena_dom INT:= ${broj_poena_domacina};
    broj_poena_g INT:= ${broj_poena_gosta};
    BEGIN
    insert into public.utakmica(id_domacina, id_gosta, broj_poena_domacina,
               broj_poena_gosta,zastavica_odigrano, broj_kola) 
               values (${id_domacina},${id_gosta}, ${broj_poena_domacina}, ${broj_poena_gosta}, 1, ${broj_kola});
               
    IF broj_poena_dom > broj_poena_g then					 
    update ekipa set bodovi = bodovi + 3 , kos_razlika = kos_razlika + broj_poena_dom - broj_poena_g
    where id_ekipe = ${id_domacina};
    
    update ekipa set kos_razlika = kos_razlika + broj_poena_g - broj_poena_dom
    where id_ekipe = ${id_gosta};
    end IF;
    
    IF broj_poena_dom < broj_poena_g then					 
    update ekipa set bodovi = bodovi + 3 , kos_razlika = kos_razlika + broj_poena_g - broj_poena_dom
    where id_ekipe = ${id_gosta};
    
    update ekipa set kos_razlika = kos_razlika + broj_poena_dom - broj_poena_g
    where id_ekipe = ${id_domacina};
    end IF ;
    end $$;`);
    res.status(200).json(results.rows)
  } catch (error) {
    res.status(404).json({message: error.message});
    console.log(error.message)
  }
}



