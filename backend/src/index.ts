import express from 'express'
import dotenv from 'dotenv'
import {createKomentar, getEkipe, getKomentari, getTablica, deleteKomentar, getUtakmice, insertUtakmica} from './db.js'
import https from 'https'
import fs from 'fs'
import cors from 'cors'
import bodyParser from 'body-parser'
const app = express()
const externalUrl = process.env.RENDER_EXTERNAL_URL;
const port = externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4080;
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
const router=express.Router();

app.use(cors())
app.get('/ekipe',getEkipe)
app.get('/tablica', getTablica)
app.get('/utakmice', getUtakmice)
app.get('/utakmice/:id_utakmice', getKomentari)
app.post('/createKomentar', createKomentar)
app.post('/insertUtakmica', insertUtakmica)
app.delete('/komentar/:id_komentara', deleteKomentar)

if (externalUrl) {
  const hostname = '127.0.0.1';
  app.listen(port, hostname, () => {
  console.log(`Server locally running at http://${hostname}:${port}/ and from
  outside on ${externalUrl}`);
  });
  }
  else{
    https.createServer({
      key: fs.readFileSync('server.key'),
      cert: fs.readFileSync('server.cert')
    }, app).listen(port, function () {
      console.log(`Server running at https://localhost:${port}/`);
    });
  }

