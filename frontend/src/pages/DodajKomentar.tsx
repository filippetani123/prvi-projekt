import React, {useState, useEffect} from 'react'
import { Formik } from "formik";
import { KomentarRequest } from '../api/types';
import { createKomentar } from '../api/ekipe';
import { Form } from "react-bootstrap";
import { useNavigate, useParams } from 'react-router';
import Navbar from '../components/Navbar';



export default function DodajKomentar() {
    const navigate = useNavigate()
    const {id_korisnika, id_utakmice} = useParams()
    const [data, setData] = useState({
      id_korisnika: id_korisnika,
      id_utakmice: id_utakmice,
      komentar:''
    });

  return (
    <div>
    <h1>Dodavanje komentara</h1>
    <Formik
      onSubmit={values => {
        console.log(values)
        createKomentar(values).then(
            response => {
                alert('Komentar uspješno dodan!')
                navigate(`/utakmice/${id_utakmice}`)
            }
        ).catch(err =>{
            navigate('/')
            console.log(values)
        })
    }}
      initialValues={data}
    >
      {({
        handleSubmit,
        handleChange,
        values,
        errors,
      }) => (

    <Form onSubmit={handleSubmit}>
        <div className="row">
            <div className="col input-width">
                <Form.Group className="mb-3" >
                    <Form.Label>Komentar</Form.Label>
                    <Form.Control type="text"
                                  name="komentar"
                                  onChange={handleChange}
                                  value={values.komentar}
                    />
                </Form.Group>
            </div>
        </div>
        <div className="pt-3 d-flex justify-content-center">
            <button className="btn btn-sm btn-success mr-1" type="submit">Unesi komentar</button>
        </div>
    </Form>)}
    </Formik>
</div>
  )
}
