import React, { useState, useEffect } from "react";
import Navbar from "../components/Navbar";
import { Link } from 'react-router-dom';
import { getRaspored } from "../api/ekipe";
import { useAuth0 } from '@auth0/auth0-react';


export default function Raspored() {
    const [loading, setLoading] = useState<boolean>(false);
    const [raspored, setRaspored] = useState<any>([])
    const { user, isAuthenticated } = useAuth0();

    useEffect(()=>{
        getRaspored().then(response =>{
            setRaspored(response)
            setLoading(true)
        }).catch(error => console.error()
        )
    }, [])

    return(
        <div>
            <div>
                <table className="table table-striped">
                <thead>
                    <tr>
                        <th style={{ width: '20%' }}>Domaćin</th>
                        <th style={{ width: '20%' }}>Gost</th>
                        <th style={{ width: '18%' }}>Rezultat</th>
                        <th style={{ width: '18%' }}>Redni broj kola</th>
                    </tr>
                </thead>
                <tbody>
                    {loading && raspored.map((utakmica: any) =>
                        
                        <tr key={utakmica.id_utakmice}>
                            
                            <td>{utakmica.ime_domacina}</td>
                            <td>{utakmica.ime_gosta}</td>
                            <th>{utakmica.broj_poena_domacina}:{utakmica.broj_poena_gosta}</th>
                            <td>{utakmica.broj_kola}</td>
                            <a href={'utakmice/'+ utakmica.id_utakmice} className="button">komentari</a>
                            </tr>
                            
                        )}
                </tbody>
            </table>

            </div>
            { (isAuthenticated && user?.email === 'dazigope@teleg.eu') && <Link to={`/insertUtakmica`} className="btn btn-sm btn-success mb-2">Unesi novu utakmicu (admin)</Link>}
        </div>
    );
}

