import React, { useState, useEffect } from "react";
import Navbar from "../components/Navbar";
import { useParams } from 'react-router-dom'
import { getKomentari, deleteKomentar } from "../api/ekipe";
import { Button } from "react-bootstrap";
import { useAuth0 } from '@auth0/auth0-react';


export default function Komentari() {
    const { user, isAuthenticated, isLoading } = useAuth0();
    const {id_utakmice} = useParams()
    const [komentari, setKomentari] = useState<any>([])
    const [loading, setLoading] = useState<boolean>(false);
    const id_korisnika : number = 1;

    function handleDelete (id: number){
        deleteKomentar(id).then(response =>{
            alert('Komentar uspješno obrisana!')
            window.location.reload()
        })
    }
    useEffect(()=>{
        getKomentari(id_utakmice).then(response =>{
            setKomentari(response)
            setLoading(true)
            console.log(komentari)
        }).catch(error => console.error()
        )
    }, [])

    return(
        <div>
            <div>
                <div>
                    {loading ? (<h1> {komentari[0]?.domacin + " : " + komentari[0]?.gost + "  -  " + komentari[0]?.broj_poena_domacina + ":"+komentari[0]?.broj_poena_gosta}</h1>) : <>Nema rezultata</>}
                    {loading && komentari.map((komentar:any)=>
                        <div className="card p-3">

                        <div className="d-flex justify-content-between align-items-center">

                        <div className="user d-flex flex-row align-items-center">

                       <span><small className="font-weight-bold text-primary">{komentar.korisnicko_ime}</small> - <small className="font-weight-bold">|"{komentar.komentar}"|</small></span>
                       <span><small className="font-weight-bold">|{'   vrijeme:' + komentar.vrijeme.substring(0,10)}|</small></span>       
                       {(user?.email === komentar.korisnicko_ime || user?.email === 'dazigope@teleg.eu')
                        && <Button variant="secondary" onClick={(e)=> handleDelete(komentar?.id_komentara)}>Obriši komentar</Button>}
                        </div>
                        </div>
                    </div>
                    )}
                </div>
                {isAuthenticated && <Button variant="contained" href={id_utakmice+'/'+user?.name}>Dodaj komentar</Button>}
            </div>
        </div>
    );
}

