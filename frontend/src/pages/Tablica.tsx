import React, { useState, useEffect } from "react";
import Navbar from "../components/Navbar";
import { getTablica } from "../api/ekipe";

export default function Tablica() {
    const [loading, setLoading] = useState<boolean>(false);
    const [tablica, setTablica] = useState<any>([])

    useEffect(()=>{
        getTablica().then(response =>{
            setTablica(response)
            setLoading(true)
        }).catch(error => console.error()
        )
    }, [])
    var pozicija : number = 1;
    return(
        <div>
            <div>
                <table className="table table-striped">
                <thead>
                    <tr>
                        <th style={{ width: '20%' }}>Pozicija</th>
                        <th style={{ width: '20%' }}>Ekipa</th>
                        <th style={{ width: '20%' }}>Broj bodova</th>
                        <th style={{ width: '18%' }}>Koš razlika</th>
                    </tr>
                </thead>
                <tbody>
                    {loading && tablica.map((ekipa: any) =>
                        <tr key={ekipa.id_ekipe}>
                            <td>{pozicija++}.</td>
                            <td>{ekipa.ime_ekipe}</td>
                            <td>{ekipa.bodovi}</td>
                            <td>{ekipa.kos_razlika}</td>
                        </tr>
                    )}
                </tbody>
            </table>

            </div>
        </div>
    );
}

