import React, {useState, useEffect} from 'react'
import { Formik } from "formik";
import { createUtakmica, getEkipe } from '../api/ekipe';
import { Form } from "react-bootstrap";
import { useNavigate, useParams } from 'react-router';
import Navbar from '../components/Navbar';



export default function UnesiUtakmicu() {
    const navigate = useNavigate()
    //const {id_korisnika, id_utakmice} = useParams()
    const [ekipe, setEkipe] = useState<any[]>([])
    const [data, setData] = useState({
      id_domacina: 1,
      id_gosta: 2,
      broj_poena_domacina:0,
      broj_poena_gosta:0,
      broj_kola:0
    });

    useEffect(()=>{
        getEkipe().then(response => setEkipe(response))
    })

  return (
    <div>
    <h1>Unos utakmice (samo admin)</h1>
    <Formik
      onSubmit={values => {
        console.log(values)
        createUtakmica(values).then(
            response => {
                alert('utakmica uspješno dodana!')
                navigate(`/utakmice`)
            }
        ).catch(err =>{
            navigate('/')
            console.log(values)
        })
    }}
      initialValues={data}
    >
      {({
        handleSubmit,
        handleChange,
        values,
        errors,
      }) => (

    <Form onSubmit={handleSubmit}>
        <div className="row">
            <div className="col input-width">
                <Form.Group className="mb-3">
                    <Form.Label>Domaćin</Form.Label>
                    <Form.Select name="id_domacina"
                                 onChange={handleChange}
                                 value={values.id_domacina}
                    >
                        {ekipe && ekipe.map((ekipa, index) =>
                            (ekipa && <option key={index} value={ekipa.id_ekipe}>{ekipa.ime_ekipe}</option>)
                        )
                        }
                    </Form.Select>
                </Form.Group>
                <Form.Group className="mb-3">
                    <Form.Label>Gost</Form.Label>
                    <Form.Select name="id_gosta"
                                 onChange={handleChange}
                                 value={values.id_gosta}
                    >
                        {ekipe && ekipe.map((ekipa, index) =>
                            (ekipa && <option key={index} value={ekipa.id_ekipe}>{ekipa.ime_ekipe}</option>)
                        )
                        }
                    </Form.Select>
                </Form.Group>
                <Form.Group className="mb-3" >
                    <Form.Label>Poeni domaćin</Form.Label>
                    <Form.Control type="number"
                                  name="broj_poena_domacina"
                                  onChange={handleChange}
                                  value={values.broj_poena_domacina}
                                 
                    />
                </Form.Group>
                <Form.Group className="mb-3" >
                    <Form.Label>Poeni gost</Form.Label>
                    <Form.Control type="number"
                                  name="broj_poena_gosta"
                                  onChange={handleChange}
                                  value={values.broj_poena_gosta}
                                 
                    />
                </Form.Group>
                <Form.Group className="mb-3" >
                    <Form.Label>Redni broj kola</Form.Label>
                    <Form.Control type="number"
                                  name="broj_kola"
                                  onChange={handleChange}
                                  value={values.broj_kola}
                                 
                    />
                </Form.Group>  
                <div className="pt-3 d-flex justify-content-center">
                    <button className="btn btn-sm btn-success mr-1" type="submit">Unesi</button>
                </div>     
            </div>
        </div>
    </Form>)}
    </Formik>
</div>
  )
}
