import axios from "axios";

const instance = axios.create({
    baseURL: "https://nba-liga.onrender.com",
    headers: {
        "Content-Type": "application/json"
    }
})

export default instance;