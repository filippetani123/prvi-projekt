export type KomentarRequest = {
    calories: number;
    date: Date;
    name: string;
    preparation: string;
    mealType: string;
}