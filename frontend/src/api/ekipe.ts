import axios from "./axios";

export async function getTablica(): Promise<any[]>{
    const response = await axios.get('/tablica');
    return response.data;
}

export async function getRaspored(): Promise<any[]>{
    const response = await axios.get('/utakmice');
    return response.data;
}

export async function getEkipe(): Promise<any[]>{
    const response = await axios.get('/ekipe');
    return response.data;
}

export async function getKomentari(id_utakmice:any): Promise<any[]>{
    const response = await axios.get(`/utakmice/${id_utakmice}`);
    return response.data;
}

export async function createKomentar(request:any): Promise<any[]>{
    const ime_korisnika = request.id_korisnika
    if(ime_korisnika === 'dazigope@teleg.eu'){
        request.id_korisnika = 2
    }else if (ime_korisnika === 'qyvozone@ema-sofia.eu'){
        request.id_korisnika = 4
    }else if (ime_korisnika === "koqihuja@teleg.eu"){
        request.id_korisnika = 5
    }
    console.log(request)
    const response = await axios.post(`/createKomentar`,request);
    return response.data;
}

export async function deleteKomentar(id_komentara:number): Promise<any[]>{
    const response = await axios.delete(`/komentar/${id_komentara}`);
    return response.data;
}

export async function createUtakmica(request:any):Promise<any[]>{
    const response = await axios.post(`/insertUtakmica`,request);
    return response.data;
}