import React from "react";
import {BrowserRouter, Routes, Route} from "react-router-dom";
import MainPage from "./pages/MainPage";
import Tablica from "./pages/Tablica";
import Raspored from "./pages/Raspored";
import Komentari from "./pages/Komentari";
import DodajKomentar from "./pages/DodajKomentar";
import UnesiUtakmicu from "./pages/UnesiUtakmicu";
import Navbar from "./components/Navbar";

export default function AppRoutes() {
    return (
        <BrowserRouter>
            <Navbar/>
            <Routes>
                <Route path='/' element={<MainPage/>} />
                <Route path='/tablica' element={<Tablica/>} />
                <Route path='/utakmice' element={<Raspored/>} />
                <Route path='/utakmice/:id_utakmice' element={<Komentari/>} />
                <Route path='/utakmice/:id_utakmice/:id_korisnika' element={<DodajKomentar/>} />
                <Route path='/insertUtakmica' element={<UnesiUtakmicu/>} />
            </Routes>
        </BrowserRouter>
    );
}