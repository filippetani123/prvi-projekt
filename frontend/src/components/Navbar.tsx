
import Login from './Login';
import Logout from './Logout';
import { useAuth0 } from '@auth0/auth0-react';

export default function Navbar(){
    const { user, isAuthenticated } = useAuth0();
    return(
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <a className="navbar-brand" href="/">NBA liga</a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
                <a className="nav-link" href="/">Početna stranica</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="/tablica">Tablica</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="/utakmice">Raspored</a>
            </li>

            </ul>
            <div className="d-flex justify-content space-between mr-4 mt-1">
            {isAuthenticated ? (
                    <div>
                        <b><span className="navbar-text">
                         Korisnik:  {user?.email}   
                        </span></b>
                        <Logout/>
                    </div>
                ) : (<Login/>)
            }
            </div>
        </div>
        </nav>
    )
}

/*
<Form className="d-flex">
                        <FormControl
                            type="search"
                            placeholder="Search"
                            className="me-2"
                            aria-label="Search"
                        />
                        <Button variant="outline-success">Search</Button>
                    </Form>
 */