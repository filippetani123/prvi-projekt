import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import {Auth0Provider} from "@auth0/auth0-react"

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement);

root.render(
  <React.StrictMode>
   <Auth0Provider
    domain="dev-wiy2m86h.eu.auth0.com"
    clientId="QzwYmlqEiVXJO6q0YZJ1gpeRk9064qmY"
    redirectUri={window.location.origin}
  >
    <App />
  </Auth0Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
